﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour
{
    public float MaxHP = 360;
    public float curHP;
    private void Awake()
    {
        curHP = MaxHP;
    }
    public virtual void TakeDamage(float damage)
    {
        curHP -= damage;
        if(curHP <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Destroy(gameObject);
        print(name + "died");
    }
}
