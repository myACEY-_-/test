﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerController : Damageable
{
    public float radius;
    List<Damageable> enemiesInRange = new List<Damageable>();

    SphereCollider triggerCollider;

    public Transform canvas;
    public Text text;

    public Transform camera;
    private void Start()
    {
        triggerCollider = GetComponent<SphereCollider>();
        triggerCollider.radius = radius;
    }
    private void OnTriggerEnter(Collider other)
    {
        Damageable enemy = GetComponent<Damageable>();
        if(enemy != null)
        {
            enemiesInRange.Add(enemy);
        }
    }

    private void Update()
    {
        canvas.LookAt(camera);
    }

    public override void TakeDamage(float damage)
    {
        text.text = (curHP - damage).ToString();
        base.TakeDamage(damage);
    }
}
