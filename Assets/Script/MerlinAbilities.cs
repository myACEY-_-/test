﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MerlinAbilities : HeroAbilities
{
    public float damage = 60f;
    public float range = 100f;

    //чем меньше, тем больше время между выстрелами
    public float basicAttacksRate = 15;

    public Transform shootPoint;
    public GameObject muzzleFlash;
    public GameObject collisionEffect;

    AnimationHandler animHandler;

    public string shootAnimationName;
    private void Start()
    {
        animHandler = GetComponentInChildren<AnimationHandler>();
    }

    public override float BasicAttack(float timer)
    {
        if (Time.time < timer)
            return 0;
        
        base.BasicAttack(timer);
        Shoot();
        return (Time.time + 1f / basicAttacksRate);
    }
    void Shoot()
    {
        RaycastHit hit;
        if(Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, range))
        {
            Debug.Log(hit.collider.gameObject.name);
            Damageable hittedEnemy = hit.collider.gameObject.GetComponent<Damageable>();
            if(hittedEnemy != null)
                hittedEnemy.TakeDamage(damage);

            //Destroy(Instantiate(muzzleFlash, shootPoint.position, Quaternion.identity), 2);
            Destroy(Instantiate(collisionEffect, hit.point, Quaternion.LookRotation(hit.normal)), 2);
        }
        animHandler.PlayAnimation(shootAnimationName);
    }
}
