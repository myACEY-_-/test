﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player: Damageable
{
    private CharacterController controller;
    
    public float speed = 6.0f;
    public float jumpHeight = 8.0f;
    private float gravity = 20.0f;

    [Range(1,5)] public float Sensivity = 1.5f;
    public float smoothBlend = 0.1f;

    private Vector3 moveDirection;
    public Transform cameraHolder;

    HeroAbilities heroAbilities;

    Animator anim;

    #region Timers
    private float nextTimeToFire = 0;
    #endregion
    private void Start()
    {
        controller = GetComponent<CharacterController>();

        Cursor.lockState = CursorLockMode.Locked;

        heroAbilities = GetComponent<HeroAbilities>();
        heroAbilities.mainCamera = cameraHolder.GetComponentInChildren<Camera>();

        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        Movement();
        CameraControls();
        Attack();
    }

    void Movement()
    {
        if (controller.isGrounded) //мы на земле, юху!
        {

            //смотрим куда двигаем ласты
            float hor = Input.GetAxis("Horizontal");
            float ver = Input.GetAxis("Vertical");
            moveDirection = new Vector3(hor, 0, ver);
            moveDirection *= speed;
            anim.SetFloat("InputX", hor, smoothBlend,Time.deltaTime);
            anim.SetFloat("InputY", ver, smoothBlend, Time.deltaTime);

            //Прыг прыг
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpHeight;
        }

        //Добавим щепотку гравитации
        moveDirection.y -= gravity * Time.deltaTime;
        //чтобы при повороте чел двигался нормальна
        moveDirection = transform.TransformDirection(moveDirection);

        //Двигать-двигать-дви-гать
        controller.Move(moveDirection * Time.deltaTime);

    }
    void CameraControls()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        Vector3 rotationY = transform.localEulerAngles;
        rotationY.y += mouseX * Sensivity;
        //transform.localRotation = Quaternion.AngleAxis(rotationY.y, Vector3.up);


        Vector3 rotationX = cameraHolder.gameObject.transform.localEulerAngles;
        rotationX.x -= mouseY * Sensivity;
        rotationX.x = Mathf.Clamp(rotationX.x, 0, 60);
        cameraHolder.gameObject.transform.localRotation = Quaternion.AngleAxis(rotationX.x, Vector3.right);
        
        transform.rotation = Quaternion.Euler(0, rotationY.y, 0);
    }

    void Attack()
    {
        if (Input.GetMouseButton(0))
        {
            float a = heroAbilities.BasicAttack(nextTimeToFire);
            if (a != 0)
            {
                nextTimeToFire = a;
            }
        }
    }
}