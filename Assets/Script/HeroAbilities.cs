﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAbilities : MonoBehaviour
{
    [HideInInspector]
    public Camera mainCamera;
    public virtual float BasicAttack(float timer)
    {
        return -1;
    }
}
